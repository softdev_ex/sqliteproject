/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onanong.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author User
 */
public class UpdateUser {
      public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            //Update
            stmt.executeUpdate("UPDATE USER set PASSWORD = 'admin127'  where ID=1");
            conn.commit();
            //Select
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER");
            
            while(rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                
                System.out.println("Id = "+ id);
                System.out.println("USERNAME = "+ username);
                System.out.println("PASSWORD = "+ password);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No libraly org.sqlite.JDBC");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connection database");
            System.exit(0);
        }
        
    }
}
