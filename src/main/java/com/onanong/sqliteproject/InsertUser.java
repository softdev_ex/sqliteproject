/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onanong.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author User
 */
public class InsertUser {
    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (1, 'admin1', 'password' );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (2, 'onanong', 'oom123' );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (3, 'mono', 'mono321' );";
            stmt.executeUpdate(sql);

           sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (4, 'morok', 'morok148' );";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.commit();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No libraly org.sqlite.JDBC");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connection database");
            System.exit(0);
        }
        System.out.println("Records created successfully");
    }
}
